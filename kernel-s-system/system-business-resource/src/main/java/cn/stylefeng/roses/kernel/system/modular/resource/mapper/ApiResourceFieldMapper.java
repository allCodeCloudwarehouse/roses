package cn.stylefeng.roses.kernel.system.modular.resource.mapper;

import cn.stylefeng.roses.kernel.system.modular.resource.entity.ApiResourceField;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 接口字段信息 Mapper 接口
 *
 * @author majianguo
 * @date 2021/05/21 15:03
 */
public interface ApiResourceFieldMapper extends BaseMapper<ApiResourceField> {

}